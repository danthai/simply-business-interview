# simply-business-interview

# Getting it up and running:
I've cloned this repo and was able to get it up and running by just calling `npm i`. Hopefully it'll be as painless for you. 

# Tech used
Given the time constraints I got this project set up using `create-react-app`. For state management I used Redux. Since there were no asynchronous event I didn't opt for using Sagas. For the CSS I opted for using a `CSS-in-JSS` approach and used Radium. This made it really quick and easy to get things looking nice and matching the design. I spent a bit of time organising the CSS so that I didn't need to keep rewriting the same styles so I've kind of built it's own utility `styles` folder and spread operators to inherit styles.

# Tests
I didn't write many tests for this project. In fact there is only one snapshot test writting for the AddTodoButton component. Given a little longer I would've wrote some more extensive snapshot tests with detailed mock data. The main Reducer has tests for all it's functionality though as they are the quickest and easiest functions to test against. 

# Extras
I've met all user stories of the brief, although I've also made it possible to handle more than one list. See the ListReducer where you'll find an extra list that has been commented out. Also I took the liberty of adding some extra functionlity to the list button, so that it shows and overview progress bar. This was quick and fun to add.

# Libaries.
Styles: Radium. Time/Date: Moment. State: Redux. Testing: Jest.

