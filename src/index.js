import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, compose, combineReducers } from 'redux'

import ListsReducer from './store/reducers/lists/ListsReducer'
import UserReducer from './store/reducers/user/UserReducer'

import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'

const composeEnhancers = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose
const rootReducer = combineReducers({
  lists: ListsReducer,
  user: UserReducer
})
const store = createStore(rootReducer, composeEnhancers())

const app = (
  <Provider store={store}>
    <App />
  </Provider>
)

ReactDOM.render(app, document.getElementById('root'))
serviceWorker.unregister()
