import React from 'react'
import Radium from 'radium'
import Moment from 'react-moment'

import styles from '../../style/components/header/header'

type headerType = {
  listTitle: string
}

const dateFormat = 'ddd DD MMMM'

const Header = (props: headerType) => {
  const { listTitle } = props
  return (
    <div style={styles.container}>
      <div style={styles.textContainer}>
        <span style={styles.listTitle}>{listTitle}</span>
        <span style={styles.date}>
          <Moment data={new Date()} format={dateFormat} />
        </span>
      </div>
    </div>
  )
}

export default Radium(Header)
