// @flow
import React from 'react'
import Radium from 'radium'
import { connect } from 'react-redux'

import type { userReducerStateType } from '../../store/reducers/user/UserReducer'
import type { listType } from '../../types/types'

import { setActiveList } from '../../store/actions/lists/ListActions'

import UserProfile from '../user-profile/UserProfile'
import ListButton from './list-button/ListButton'

import styles from '../../style/components/list-panel/list-panel'

type listPanelType = {
  user: userReducerStateType,
  lists: Array<listType>,
  activeList: number,
  setActiveList: (index: number) => any
}

const ListPanel = (props: listPanelType) => {
  const {
    user,
    lists,
    activeList
  } = props
  const renderLists = (lists, activeList) => {
    return lists.map((list, index) =>
      <ListButton
        {...list}
        key={`list_button_${index}`}
        isActive={index === activeList}
        handleClick={() => { props.setActiveList(index) }}
      />
    )
  }
  return (
    <div style={styles}>
      <UserProfile user={user} />
      {lists && renderLists(lists, activeList)}
    </div>
  )
}

const mapStateToProps = state => ({
  user: state.user,
  lists: state.lists.lists,
  activeList: state.lists.activeList
})

const mapDispatchToProps = dispatch => ({
  setActiveList: listId => dispatch(setActiveList(listId))
})

export default connect(mapStateToProps, mapDispatchToProps)(Radium(ListPanel))
