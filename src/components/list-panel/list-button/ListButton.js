// @flow
import React from 'react'
import Radium from 'radium'

import { IoIosList } from 'react-icons/io'

import styles from '../../../style/components/list-button/list-button'

import type { listType } from '../../../types/types'

type listButtonType = listType & {
  id: Number,
  isActive: boolean,
  handleClick: (id: Number) => any
}

const ListButton = (props: listButtonType) => {
  const {
    title,
    id,
    tasks,
    isActive,
    handleClick
  } = props
  const tasksComplete = tasks.filter(task => task.done).length
  const percentageComplete = Math.floor((tasksComplete / tasks.length) * 100)
  return (
    <div style={[styles.container, isActive ? styles.container.isActive : null]}>
      <div style={styles.buttonContainer}>
        <IoIosList style={styles.icon} size={23} />
        <button style={styles.button} onClick={() => handleClick(id)}>{title}</button>
      </div>
      <div style={styles.progressContainer}>
        <span style={[styles.progressTxt, percentageComplete === 100 ? styles.progressTxt.complete : null]}>
          {`${tasksComplete}/${tasks.length}`}
        </span>
        <div style={styles.progressBar}>
          <div style={[styles.progressBar, styles.progressBar.bg]} />
          <div style={[styles.progressBar, styles.progressBar.progress, { width: `${percentageComplete}%` }]} />
        </div>
      </div>
    </div>
  )
}

export default Radium(ListButton)
