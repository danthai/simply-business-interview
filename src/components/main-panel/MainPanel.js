// @flow
import React from 'react'
import { connect } from 'react-redux'
import Radium from 'radium'

import type { listType, taskType } from '../../types/types'
import {
  markTaskDone,
  markTaskIncomplete,
  deleteTask,
  addTask,
  updateTaskLabel
} from '../../store/actions/tasks/TaskActions'

import Header from '../header/Header'
import Task from './tasks/Task'
import AddTodoButton from '../add-todo/AddTodoButton'

import styles from '../../style/components/main-panel/main-panel'

type mainPanelType = {
  list: listType,
  tasks: Array<taskType>,
  markTaskDone: (taskId: Number) => any,
  markTaskIncomplete: (taskId: Number) => any,
  deleteTask: (taskId: Number) => any,
  updateTaskLabel: (taskId: Number, label: string) => any,
  addTask: () => any
}

const MainPanel = (props: mainPanelType) => {
  const {
    list,
    tasks
  } = props
  const renderTasks = () => props.tasks.map((task, index) =>
    <Task
      key={index}
      task={task}
      taskId={index}
      onMarkDone={(taskId) => props.markTaskDone(taskId)}
      onMarkIncomplete={(taskId) => props.markTaskIncomplete(taskId)}
      onDeleteTask={(taskId) => props.deleteTask(taskId)}
      updateTaskLabel={(taskId, label) => props.updateTaskLabel(taskId, label)}
    />)

  return (
    <div style={styles.container}>
      <Header listTitle={list.title} />
      <div style={styles.tasksWrapper}>
        { tasks && renderTasks() }
      </div>
      <div style={styles.addTodoContainer}>
        <AddTodoButton handleClick={props.addTask} />
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  const { activeList, lists } = state.lists
  return {
    list: lists[activeList],
    tasks: lists[activeList].tasks,
    activeList: activeList
  }
}

const mapDispatchToProps = dispatch => ({
  markTaskDone: (task) => dispatch(markTaskDone(task)),
  markTaskIncomplete: (task) => dispatch(markTaskIncomplete(task)),
  deleteTask: (task) => dispatch(deleteTask(task)),
  addTask: () => dispatch(addTask()),
  updateTaskLabel: (task, label) => dispatch(updateTaskLabel(task, label))
})

export default connect(mapStateToProps, mapDispatchToProps)(Radium(MainPanel))
