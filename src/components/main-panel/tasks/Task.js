// @flow
import React, { Component } from 'react'
import Radium from 'radium'

import { IoMdSquareOutline, IoIosCheckbox } from 'react-icons/io'
import { TiTrash } from 'react-icons/ti'

import type { taskType } from '../../../types/types'

import COLOURS from '../../../style/colours/colours'
import DISPLAY from '../../../style/display/display'
import styles from '../../../style/components/tasks/task'

type taskPropsType = {
  task: taskType,
  taskId: Number,
  onMarkDone: (taskId: Number) => any,
  onMarkIncomplete: (taskId: Number) => any,
  onDeleteTask: (taskId: Number) => any,
  updateTaskLabel: (taskId: Number, label: string) => any
}

type taskStateType = {
  hover: boolean,
  todoEditLabel: string|null
}

class Task extends Component<taskPropsType, taskStateType> {
  state = {
    hover: false,
    todoEditLabel: null
  }
  handleRollover = () =>
    this.setState({
      hover: true
    })
  handleRollOff = () =>
    this.setState({
      hover: false
    })
  renderDoneIcon = () => {
    const { onMarkIncomplete, taskId } = this.props
    return (
      <button
        onClick={() => onMarkIncomplete(taskId)}
        style={styles.button}>
        <IoIosCheckbox style={styles.checkbox} color={COLOURS.COMPLETE.STANDARD} size={32} />
      </button>
    )
  }
  renderIcon = () => {
    const { onMarkDone, taskId } = this.props
    return (
      <button
        onClick={() => onMarkDone(taskId)}
        style={styles.button}>
        <IoMdSquareOutline style={styles.checkbox} color={COLOURS.GREY.STANDARD} size={32} />
      </button>
    )
  }
  renderTrashIcon = () => {
    const { onDeleteTask, taskId, task: { label } } = this.props
    return (
      <div
        key={`trash_${label}`}
        style={[styles.trash, this.state.hover ? DISPLAY.BLOCK : null]}
        onClick={() => onDeleteTask(taskId)}>
        <TiTrash size={28} />
      </div>
    )
  }
  renderLabel = () => {
    const { task: { label, done } } = this.props
    return <span style={[done ? styles.label.complete : styles.label.incomplete]}>{label}</span>
  }
  handleEditComplete = (e: any) => {
    e.preventDefault()
    const { updateTaskLabel, taskId } = this.props
    updateTaskLabel(taskId, this.state.todoEditLabel || '')
    this.setState({
      todoEditLabel: null
    })
  }
  handleEditChange = (e) =>
    this.setState({
      todoEditLabel: e.currentTarget.value
    })
  renderInput = () => {
    const { task: { label } } = this.props
    return (
      <form onSubmit={(e) => this.handleEditComplete(e)}>
        <input
          onChange={this.handleEditChange}
          onBlur={(e) => this.handleEditComplete(e)}
          style={styles.input}
          value={this.state.todoEditLabel || label}
        />
      </form>
    )
  }
  render () {
    const { task: { done, edit } } = this.props
    return (
      <div
        onPointerLeave={this.handleRollOff}
        onPointerEnter={this.handleRollover}
        style={[styles.container, edit ? styles.container.edit : null]}>
        <div style={styles.taskContainer}>
          { done ? this.renderDoneIcon() : this.renderIcon() }
          { edit ? this.renderInput() : this.renderLabel() }
        </div>
        { this.renderTrashIcon() }
      </div>
    )
  }
}

export default (Radium(Task))
