import React from 'react'
import Radium from 'radium'

import type { userReducerStateType } from '../../store/reducers/user/UserReducer'

import styles from '../../style/components/user-profile/user-profile'

const UserProfile = ({ user: { img, name } }: userReducerStateType) =>
  <div style={styles.base}>
    <img style={styles.img} src={`/users/images/${img}.png`} />
    <span style={styles.txt}>{name}</span>
  </div>

export default Radium(UserProfile)
