import React from 'react'
import Radium from 'radium'

import { IoMdAddCircle } from 'react-icons/io'

import styles from '../../style/components/add-todo/add-todo'

type addTodoButtonType = {
  handleClick: () => any
}

const AddTodoButton = (props: addTodoButtonType) => {
  const {
    handleClick
  } = props
  return (
    <button onClick={handleClick} style={styles.todoContainer}>
      <IoMdAddCircle
        style={styles.icon}
        size={30} />
      <span style={styles.label}>Add a to-do</span>
    </button>
  )
}

export default Radium(AddTodoButton)
