import React from 'react'
import * as ShallowRenderer from 'react-test-renderer/shallow'

import AddTodoButton from './AddTodoButton'

describe.only('AddTodoButton', () => {
  const tree = ShallowRenderer.createRenderer()
  it('renders correctly default', () => {
    expect(tree.render(<AddTodoButton handleClick={() => {}} />)).toMatchSnapshot()
  })
})
