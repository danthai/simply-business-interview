import SIZE from '../size/size'

export const MARGIN = {
  BOTTOM: {
    STANDARD: {
      margin: `0 0 ${SIZE.STANDARD} 0`
    }
  },
  RIGHT: {
    SMALL: {
      margin: `0 ${SIZE.SMALL} 0 0`
    },
    LARGE: {
      margin: `0 ${SIZE.LARGE} 0 0`
    }
  },
  FULL: {
    STANDARD: {
      margin: `0 ${SIZE.LARGE} ${SIZE.STANDARD} ${SIZE.LARGE}`
    }
  }
}

export default MARGIN
