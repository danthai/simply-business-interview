export const TEXT_DECORATION = {
  LINE_THROUGH: 'line-through'
}

export default TEXT_DECORATION
