export const FLEX = {
  COLUMN: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  ROW: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  ALIGN_SELF: {
    FLEX_END: 'flex-end'
  },
  JUSTIFY_CONTENT: {
    SPACE_BETWEEN: 'space-between',
    FLEX_END: 'flex-end',
    FLEX_START: 'flex-start'
  }
}

export default FLEX
