import PADDING from '../padding/padding'
import BORDER from '../border/border'
import COLOURS from '../colours/colours'
import CURSOR from '../cursor/cursor'

export const BUTTON = {
  INVISIBLE: {
    ...PADDING.NONE,
    border: BORDER.NONE,
    backgroundColor: COLOURS.BUTTONS.TRANSPARENT,
    cursor: CURSOR.POINTER
  }
}

export default BUTTON
