import CURSOR from '../cursor/cursor'
import COLOURS from '../colours/colours'

export const LINKS = {
  ACTION: {
    cursor: CURSOR.POINTER,
    color: COLOURS.ACTION.BASE,
    ':hover': {
      color: COLOURS.ACTION.HOVER
    }
  }
}

export default LINKS
