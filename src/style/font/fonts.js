export const FONTS = {
  SIZES: {
    XSMALL: '12px',
    SMALL: '14px',
    STANDARD: '18px',
    LARGE: '28px',
    XLARGE: '34px'
  }
}

export default FONTS
