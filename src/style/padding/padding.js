export const PADDING = {
  NONE: {
    padding: 'none'
  },
  STANDARD: {
    padding: 6,
    boxSizing: 'border-box'
  },
  LARGE: {
    padding: 15,
    boxSizing: 'border-box'
  }
}

export default PADDING
