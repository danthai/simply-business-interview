export const BORDER = {
  RADIUS: {
    STANDARD: 4
  },
  NONE: 'none'
}

export default BORDER
