export const CURSOR = {
  DEFAULT: 'default',
  POINTER: 'pointer'
}

export default CURSOR
