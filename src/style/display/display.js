export const DISPLAY = {
  BLOCK: {
    display: 'BLOCK'
  },
  NONE: {
    display: 'NONE'
  }
}

export default DISPLAY
