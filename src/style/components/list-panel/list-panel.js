import * as STYLES from '../../index'

const styles = {
  width: '40%',
  height: '100%',
  background: STYLES.COLOURS.BACKGROUND.BASE,
  borderRight: `solid 1px ${STYLES.COLOURS.GREY.STANDARD}`,
  float: 'left',
  boxSizing: 'border-box'
}

export default styles
