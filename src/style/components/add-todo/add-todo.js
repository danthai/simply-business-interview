import * as STYLES from '../../index'

const styles = {
  todoContainer: {
    ...STYLES.FLEX.ROW,
    ...STYLES.PADDING.STANDARD,
    ...STYLES.MARGIN.FULL.STANDARD,
    ...STYLES.LINKS.ACTION,
    border: STYLES.BORDER.NONE,
    background: STYLES.COLOURS.BACKGROUND.NONE,
    height: 54
  },
  icon: {
    marginRight: STYLES.SIZE.STANDARD
  },
  label: {
    fontWeight: 600,
    fontSize: STYLES.FONTS.SIZES.STANDARD
  }
}

export default styles
