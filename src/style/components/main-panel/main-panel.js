import * as STYLES from '../../index'

const styles = {
  container: {
    ...STYLES.FLEX.COLUMN,
    width: '60%',
    float: 'right'
  },
  tasksWrapper: {
    margin: '8px 8px 8px 16px',
    height: 280,
    overflowY: 'auto'
  },
  addTodoContainer: {
    backgroundColor: STYLES.COLOURS.GREY.LIGHT,
    flexGrow: 1
  }
}

export default styles
