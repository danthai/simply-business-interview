import * as STYLES from '../../index'

const styles = {
  base: {
    ...STYLES.FLEX.ROW,
    ...STYLES.PADDING.LARGE,
    justifyContent: STYLES.FLEX.JUSTIFY_CONTENT.FLEX_START
  },
  img: { ...STYLES.MARGIN.RIGHT.LARGE },
  txt: {
    fontWeight: 600,
    fontSize: STYLES.FONTS.SIZES.STANDARD
  }
}

export default styles
