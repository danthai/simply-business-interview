import * as STYLES from '../../index'

const styles = {
  container: {
    ...STYLES.FLEX.ROW,
    ...STYLES.PADDING.STANDARD,
    ...STYLES.MARGIN.BOTTOM.STANDARD,
    backgroundColor: STYLES.COLOURS.BACKGROUND.BASE,
    borderRadius: STYLES.BORDER.RADIUS.STANDARD,
    ':hover': { backgroundColor: STYLES.COLOURS.BACKGROUND.HOVER },
    edit: { backgroundColor: STYLES.COLOURS.BACKGROUND.EDIT }
  },
  button: STYLES.BUTTON.INVISIBLE,
  taskContainer: {
    ...STYLES.FLEX.ROW,
    cursor: STYLES.CURSOR.DEFAULT
  },
  checkbox: {
    ...STYLES.MARGIN.RIGHT.LARGE
  },
  trash: {
    ...STYLES.MARGIN.RIGHT.SMALL,
    ...STYLES.LINKS.ACTION,
    ...STYLES.DISPLAY.NONE,
    alignSelf: STYLES.FLEX.ALIGN_SELF.FLEX_END
  },
  input: {
    width: 300,
    fontSize: STYLES.FONTS.SIZES.STANDARD
  },
  label: {
    incomplete: {
      fontSize: STYLES.FONTS.SIZES.STANDARD,
      color: STYLES.COLOURS.TEXT.STANDARD
    },
    complete: {
      color: STYLES.COLOURS.TEXT.DISABLED,
      textDecoration: STYLES.TEXT_DECORATION.LINE_THROUGH
    }
  }
}

export default styles
