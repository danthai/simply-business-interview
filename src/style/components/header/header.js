import * as STYLES from '../../index'

export const HEADER = {
  container: {
    height: 185,
    backgroundImage: `url(/assets/list-bg.png)`,
    position: 'relative'
  },
  textContainer: {
    ...STYLES.FLEX.COLUMN,
    position: 'absolute',
    top: 100,
    left: 20,
    height: 65
  },
  listTitle: {
    color: STYLES.COLOURS.TEXT.INVERTED,
    fontSize: STYLES.FONTS.SIZES.XLARGE,
    fontWeight: 200,
    letterSpacing: 2.3
  },
  date: {
    color: STYLES.COLOURS.TEXT.INVERTED,
    fontSize: STYLES.FONTS.SIZES.SMALL,
    fontWeight: 400
  }
}

export default HEADER
