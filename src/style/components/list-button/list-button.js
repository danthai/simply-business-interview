import * as STYLES from '../../index'

const styles = {
  container: {
    ...STYLES.PADDING.LARGE,
    ...STYLES.MARGIN.FULL.STANDARD,
    borderRadius: STYLES.BORDER.RADIUS.STANDARD,
    border: STYLES.BORDER.NONE,
    isActive: {
      backgroundColor: STYLES.COLOURS.BACKGROUND.HOVER
    }
  },
  buttonContainer: {
    ...STYLES.FLEX.ROW,
    justifyContent: STYLES.FLEX.JUSTIFY_CONTENT.FLEX_START
  },
  button: {
    background: STYLES.COLOURS.BACKGROUND.NONE,
    border: STYLES.BORDER.NONE,
    fontSize: STYLES.FONTS.SIZES.STANDARD,
    fontWeight: 600,
    cursor: STYLES.CURSOR.POINTER
  },
  icon: {
    ...STYLES.MARGIN.RIGHT.LARGE,
    color: STYLES.COLOURS.TEXT.DISABLED
  },
  progressContainer: {
    ...STYLES.FLEX.ROW,
    justifyContent: STYLES.FLEX.JUSTIFY_CONTENT.FLEX_START
  },
  progressTxt: {
    ...STYLES.PADDING.STANDARD,
    marginRight: 10,
    marginTop: 6,
    fontWeight: 600,
    fontSize: STYLES.FONTS.SIZES.XSMALL,
    letterSpacing: '0.15em',
    backgroundColor: STYLES.COLOURS.BACKGROUND.BASE,
    color: STYLES.COLOURS.TEXT.DISABLED,
    borderRadius: STYLES.BORDER.RADIUS.STANDARD,
    complete: {
      ...STYLES.PADDING.STANDARD,
      color: STYLES.COLOURS.TEXT.INVERTED,
      fontSize: STYLES.FONTS.SIZES.XSMALL,
      fontWeight: 800,
      backgroundColor: STYLES.COLOURS.COMPLETE.STANDARD,
      borderRadius: STYLES.BORDER.RADIUS.STANDARD
    }
  },
  progressBar: {
    margin: '4px 0 2px 0',
    boxSizing: 'border-box',
    height: 2,
    position: 'relative',
    width: '87%',
    bg: {
      position: 'absolute',
      top: 0,
      left: 0,
      backgroundColor: STYLES.COLOURS.GREY.STANDARD,
      width: '100%'
    },
    progress: {
      position: 'absolute',
      backgroundColor: STYLES.COLOURS.COMPLETE.STANDARD,
      top: 0,
      left: 0,
      transition: 'width 0.3s'
    }
  }
}

export default styles
