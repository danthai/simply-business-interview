export const SIZE = {
  SMALL: '2px',
  STANDARD: '5px',
  LARGE: '10px'
}

export default SIZE
