export const COLOURS = {
  ACTION: {
    BASE: '#3c1ccc',
    HOVER: '#6e5bc5'
  },
  BUTTONS: {
    TRANSPARENT: 'transparent'
  },
  BACKGROUND: {
    BASE: 'white',
    EDIT: '#f3effe',
    HOVER: '#f3effe',
    NONE: 'none'
  },
  TEXT: {
    STANDARD: '#222222',
    DISABLED: '#777777',
    INVERTED: 'white'
  },
  GREY: {
    LIGHT: '#f3f3f3',
    STANDARD: '#CCC'
  },
  COMPLETE: {
    STANDARD: '#61c66f'
  }
}

export default COLOURS
