import React, { Component } from 'react'
import Radium from 'radium'

import ListPanel from './components/list-panel/ListPanel'
import MainPanel from './components/main-panel/MainPanel'

class App extends Component {
  render () {
    const style = {
      width: 790,
      height: 542,
      marginLeft: 'auto',
      marginRight: 'auto',
      marginTop: 100,
      borderRadius: 10,
      boxShadow: '2px 4px 8px 2px rgba(0, 0, 0, 0.2)',
      overflow: 'hidden'
    }
    return (
      <div style={style}>
        <ListPanel />
        <MainPanel />
      </div>
    )
  }
}

export default Radium(App)
