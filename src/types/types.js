// @flow
export type actionType = {
  payload: any,
  type: string
}

export type taskType = {
  label: string,
  done: boolean,
  edit: boolean
}

export type listType = {
  title: string,
  tasks: Array<taskType>
}

export type taskLabelUpdateType = {
  taskId: number,
  label: string
}
