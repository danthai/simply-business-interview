// @flow
import type { actionType, listType, taskLabelUpdateType } from '../../../types/types'
import {
  MARK_TASK_DONE,
  MARK_TASK_INCOMPLETE,
  DELETE_TASK,
  ADD_TASK,
  UPDATE_TASK_LABEL,
  SET_ACTIVE_LIST
} from '../../actions/ActionTypes'

type listReducerStateType = {
  activeList: number,
  lists: Array<listType>
}

export const initialState: listReducerStateType = {
  activeList: 0,
  lists: [
    {
      title: 'Team To-Do List',
      tasks: [
        {
          label: 'Shortlist features for MVP',
          done: false,
          edit: false
        },
        {
          label: 'Launch PPC campaign with new creative',
          done: false,
          edit: false
        },
        {
          label: 'Define audience breakdown with new data',
          done: false,
          edit: false
        },
        {
          label: 'Launch demo page for SEO analysis',
          done: true,
          edit: false
        }
      ]
    }
    // {
    //   title: 'This is the second list',
    //   tasks: [
    //     {
    //       label: 'Another meaningless task',
    //       done: false,
    //       edit: false
    //     }
    //   ]
    // }
  ]
}

const reducer = (state: listReducerStateType = initialState, { payload, type }: actionType): listReducerStateType => {
  switch (type) {
    case MARK_TASK_DONE: return markTaskDone(state, payload)
    case MARK_TASK_INCOMPLETE: return markTaskIncomplete(state, payload)
    case DELETE_TASK: return deleteTask(state, payload)
    case ADD_TASK: return addTask(state)
    case UPDATE_TASK_LABEL: return updateTaskLabel(state, payload)
    case SET_ACTIVE_LIST: return setActiveList(state, payload)
    default: return state
  }
}

const setActiveList = (state: listReducerStateType, listId: Number) => {
  return {
    ...state,
    activeList: listId
  }
}

const markTaskDone = (state: listReducerStateType, taskId: number) => {
  const { activeList } = state
  const updatedTasks = [...state.lists[activeList].tasks]
  updatedTasks[taskId] = {
    ...state.lists[activeList].tasks[taskId],
    done: true
  }
  const updatedLists = [...state.lists]
  updatedLists[activeList].tasks = [...updatedTasks]
  return {
    ...state,
    lists: [...updatedLists]
  }
}

const markTaskIncomplete = (state: listReducerStateType, taskId: number) => {
  const { activeList } = state
  const updatedTasks = [...state.lists[activeList].tasks]
  updatedTasks[taskId] = {
    ...state.lists[activeList].tasks[taskId],
    done: false
  }
  const updatedLists = [...state.lists]
  updatedLists[activeList].tasks = [...updatedTasks]
  return {
    ...state,
    lists: [...updatedLists]
  }
}

const deleteTask = (state: listReducerStateType, taskId: number) => {
  const { activeList } = state
  const updatedTasks = [...state.lists[activeList].tasks].filter((task, index) => {
    return index !== taskId
  })
  const updatedLists = [...state.lists]
  updatedLists[activeList].tasks = [...updatedTasks]
  return {
    ...state,
    lists: [...updatedLists]
  }
}

const addTask = (state: listReducerStateType) => {
  const { activeList } = state
  const newTask = {
    label: '',
    done: false,
    edit: true
  }
  const updatedLists = [...state.lists]
  updatedLists[activeList].tasks = [...state.lists[activeList].tasks].concat([newTask])
  return {
    ...state,
    lists: updatedLists
  }
}

const updateTaskLabel = (state: listReducerStateType, { taskId, label }: taskLabelUpdateType) => {
  const { activeList } = state
  const updatedTasks = [...state.lists[activeList].tasks]
  const currentTask = state.lists[activeList].tasks[taskId]
  console.log('Current task: ', currentTask)
  updatedTasks[taskId] = {
    ...currentTask,
    label: label === '' || !label ? 'Empty task' : label,
    edit: false,
    done: currentTask.done
  }
  const updatedLists = [...state.lists]
  updatedLists[activeList].tasks = [...updatedTasks]
  return {
    ...state,
    lists: [...updatedLists]
  }
}

export default reducer
