import reducer from './ListsReducer'
import { mockState, mockStateComplete, mockStateUpdateLabel } from './ListReducer.mock'
import {
  MARK_TASK_DONE,
  MARK_TASK_INCOMPLETE,
  ADD_TASK,
  DELETE_TASK,
  UPDATE_TASK_LABEL,
  SET_ACTIVE_LIST
} from '../../actions/ActionTypes'

describe('ListReducer', () => {
  describe('MARK_TASK_DONE', () => {
    it('should mark task done correctly', () => {
      expect(reducer(
        mockState,
        { type: MARK_TASK_DONE, payload: 0 }
      )).toMatchObject({
        activeList: 0,
        lists: [
          {
            title: 'List',
            tasks: [
              {
                label: 'test',
                done: true,
                edit: false
              }
            ]
          }
        ]
      })
    })
  })
  describe('MARK_TASK_INCOMPLETE', () => {
    it('should mark task as incomplete', () => {
      expect(reducer(
        mockStateComplete,
        { type: MARK_TASK_INCOMPLETE, payload: 0 }
      )).toMatchObject({
        activeList: 0,
        lists: [
          {
            title: 'List',
            tasks: [
              {
                label: 'test',
                done: false,
                edit: false
              }
            ]
          }
        ]
      })
    })
  })
  describe('ADD_TASK', () => {
    it('should add a new task', () => {
      expect(reducer(mockStateComplete, { type: ADD_TASK })).toMatchObject({
        activeList: 0,
        lists: [
          {
            title: 'List',
            tasks: [
              {
                label: 'test',
                done: false,
                edit: false
              },
              {
                label: '',
                done: false,
                edit: true
              }
            ]
          }
        ]
      })
    })
  })
  describe('DELETE_TASK', () => {
    it('should delete a task', () => {
      expect(reducer(mockState, { type: DELETE_TASK, payload: 0 })).toMatchObject({
        activeList: 0,
        lists: [
          {
            title: 'List',
            tasks: []
          }
        ]
      })
    })
  })
  describe('UPDATE_TASK_LABEL', () => {
    it('should update task label', () => {
      expect(reducer(mockStateUpdateLabel, { type: UPDATE_TASK_LABEL, payload: { taskId: 0, label: 'New title' } })).toMatchObject({
        activeList: 0,
        lists: [
          {
            title: 'List',
            tasks: [
              {
                label: 'New title',
                done: false,
                edit: false
              }
            ]
          }
        ]
      })
    })
  })
  describe('SET_ACTIVE_LIST', () => {
    it('should set the current list id', () => {
      expect(reducer(mockState, { type: SET_ACTIVE_LIST, payload: 1 }).activeList).toEqual(1)
    })
  })
})
