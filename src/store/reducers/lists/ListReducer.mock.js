export const mockState = {
  activeList: 0,
  lists: [
    {
      title: 'List',
      tasks: [
        {
          label: 'test',
          done: false,
          edit: false
        }
      ]
    }
  ]
}

export const mockStateComplete = {
  activeList: 0,
  lists: [
    {
      title: 'List',
      tasks: [
        {
          label: 'test',
          done: false,
          edit: false
        }
      ]
    }
  ]
}

export const mockStateUpdateLabel = {
  activeList: 0,
  lists: [
    {
      title: 'List',
      tasks: [
        {
          label: 'Boring Label',
          done: false,
          edit: true
        }
      ]
    }
  ]
}
