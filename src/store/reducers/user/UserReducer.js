// @flow
import type { actionType } from '../../../types/types'

export type userReducerStateType = {
  img: string,
  name: string
}

const initialState: userReducerStateType = {
  img: 'dan_sanderson',
  name: 'Dan Sanderson'
}

const reducer = (state: userReducerStateType = initialState, { type, payload }: actionType) => {
  switch (type) {
    default: return state
  }
}

export default reducer
