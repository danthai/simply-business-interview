import type { actionType } from '../ActionTypes'
import {
  MARK_TASK_DONE,
  MARK_TASK_INCOMPLETE,
  DELETE_TASK,
  ADD_TASK,
  UPDATE_TASK_LABEL
} from '../ActionTypes'

import { taskSelectionType } from '../../../types/types'

export const markTaskDone = (task: taskSelectionType): actionType => ({
  type: MARK_TASK_DONE,
  payload: task
})

export const markTaskIncomplete = (task: taskSelectionType): actionType => ({
  type: MARK_TASK_INCOMPLETE,
  payload: task
})

export const deleteTask = (task: taskSelectionType): actionType => ({
  type: DELETE_TASK,
  payload: task
})

export const addTask = (): actionType => ({
  type: ADD_TASK
})

export const updateTaskLabel = (taskId: Number, label: string): actionType => ({
  type: UPDATE_TASK_LABEL,
  payload: {
    taskId,
    label
  }
})
