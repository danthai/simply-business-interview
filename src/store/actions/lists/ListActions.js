import type { actionType } from '../ActionTypes'
import { SET_ACTIVE_LIST } from '../ActionTypes'

export const setActiveList = (listId: Number): actionType => {
  return {
    type: SET_ACTIVE_LIST,
    payload: listId
  }
}
