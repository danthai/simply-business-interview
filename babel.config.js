module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        debug: false,
        useBuiltIns: 'usage'
      }
    ],
    '@babel/preset-flow',
    '@babel/preset-react'
  ],
  plugins: [
    '@babel/plugin-proposal-export-default-from'
  //   [
  //     '@babel/plugin-proposal-object-rest-spread',
  //     {
  //       useBuiltIns: true
  //     }
  //   ],
  //   ['@babel/plugin-proposal-class-properties'],
  //   ['@babel/plugin-transform-modules-commonjs']
  ]
  // env: {
  //   __TEST__: {
  //     plugins: ['istanbul']
  //   },
  //   server: {
  //     presets: [
  //       [
  //         '@babel/preset-env',
  //         {
  //           debug: false,
  //           targets: {
  //             node: 'current'
  //           }
  //         }
  //       ]
  //     ]
  //   }
  // }
}
